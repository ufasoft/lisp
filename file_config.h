/*######     Copyright (c) 1997-2012 Ufasoft  http://ufasoft.com  mailto:support@ufasoft.com    ##########################################
# This program is free software; you can redistribute it and/or modify it under the terms of the GNU General Public License as published #
# by the Free Software Foundation; either version 3, or (at your option) any later version. This program is distributed in the hope that #
# it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. #
# See the GNU General Public License for more details. You should have received a copy of the GNU General Public License along with this #
# program; If not, see <http://www.gnu.org/licenses/>                                                                                    #
########################################################################################################################################*/
#if defined(__unix__) || defined(_M_ARM)
//#	define UCFG_EXTENDED 0
#	define UCFG_GUI 0
#	define UCFG_XML 0
#	define UCFG_STLSOFT 0
#	define UCFG_USE_NTL 1
#endif


#define VER_FILEDESCRIPTION_STR "lisp"
#define VER_INTERNALNAME_STR "lisp"
#define VER_ORIGINALFILENAME_STR "lisp.exe"


